import subprocess
import signal
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--dev', nargs='+', type=int)
parser.add_argument("-i", "--ip", type=str)
args = parser.parse_args()
print("N:", args.dev)

subprocess.call("pkill gst-launch-1.0", shell=True)

gst_processes = []

for num, cam_no in enumerate(args.dev):
    command = 'gst-launch-1.0 v4l2src device=/dev/video{} ! image/jpeg,width=1280,height=720,framerate=30/1 ! vaapijpegdec ! vaapih264enc bitrate=2000 ! rtph264pay pt=96 ! udpsink port={} host={}'.format(cam_no, 8000 + num, args.ip)
    print(command)
    gst_processes.append(subprocess.Popen(command, shell=True))

try:
    gst_processes[0].wait()
except KeyboardInterrupt:
    for proc in gst_processes:
        proc.send_signal(signal.SIGINT)