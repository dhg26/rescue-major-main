# About our project
For detailed documentation and explanation of our project, please read the wiki on this repository. 

# User instructions
This is the main repository to add anything Rescue Major related to that isn't a
ROS Package - ROS Packages go in the repo [here](https://gitlab.developers.cam.ac.uk/curobotics/rescue-major/rescue-major-ros-packages). 

Email [rcs62@cam.ac.uk](mailto:rcs62@cam.ac.uk) if you have any problems.

### Connecting to the NUC
- Startup the NUC
- Wait about for the system to start up
- SSH into NUC: ssh robotics@roboticsnuc.jjurm.com

# Quick Resources

## Photos and videos
Photos and videos can be found here: https://tinyurl.com/cur-rescue

## CAD files
**Mk.III** (Under development): https://a360.co/3h6fQ7W 

**Mk.II**  (Current): https://a360.co/337Xmi9

**Mk.I**   (Depreciated): https://a360.co/2Jw9zFw

Email [kcj21@cam.ac.uk](mailto:kcj21@cam.ac.uk) if you have any problems.

## PCB design
KiCad PCB Designs can be found here: https://tinyurl.com/cur-rescue-pcb