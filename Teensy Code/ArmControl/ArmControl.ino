/*
 * SOME INSTRUCTIONS:
 * 
 * Update a float array argument fed into updateMotorPower to move the two links in the arm
 * Update a float argument fed into updateBasePosition to move the base 
 * Set initArmDown to true to move the arm to the datum position before any operation
 * 
 * The position of the two arm links can be read from encoderAngle, a float array of size 2
 * The position of the base can be read from baseAngle. 
 * 
 * The angular velocities of two links can be read from angularVelocity but it's a bit dodgy at the moment.
 */

#include <ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Bool.h>

#include "config.h"
#include "controller.h"

// ROS
ros::NodeHandle nh;

std_msgs::Float32MultiArray arm_values;

ros::Publisher pub_arm_values("/arm_current_angles", &arm_values);

// format: [base, arm1, arm2]
float motor_data[3] = {0, 0, 0};

const int ledPin = 13;

void messageArm( const std_msgs::Float32MultiArray& msg){
  if (msg.data_length < 3) return;

  // blink if values changed
  bool changed = false;
  for (int i = 0; i < 3; i++) {
    if (motor_data[i] != msg.data[i]) changed = true;
  }
  if (changed) {
    blinkLed();
  }
  
  for (int i = 0; i < 3; i++) {
    motor_data[i] = msg.data[i];
  }
}

ros::Subscriber<std_msgs::Float32MultiArray> subscriberArm("/arm_demand_angles", &messageArm);

void zeroArmRequestCallback(const std_msgs::Bool& msg){
  if(msg.data == true){
    moveToZero();
  }
}

ros::Subscriber<std_msgs::Bool> subscriberZeroArm("/zero_arm_request", &zeroArmRequestCallback);

int prevMillis;

void setup() {
  nh.initNode();
  
  pinSetting();
  pinMode(ledPin, OUTPUT);
  enableAllMotors();

  // resetting some timers here
  mainTime = millis();
  loopCountTime = micros();
  printTime = millis();
  prevMillis = millis();

  arm_values.layout.dim = (std_msgs::MultiArrayDimension *)
  malloc(sizeof(std_msgs::MultiArrayDimension)*2);
  arm_values.layout.dim[0].label = "height";
  arm_values.layout.dim[0].size = 3;
  arm_values.layout.dim[0].stride = 1;
  arm_values.layout.data_offset = 0;
  arm_values.data = (float *)malloc(sizeof(float)*8);
  arm_values.data_length = 3;
  
  nh.subscribe(subscriberArm);
  nh.subscribe(subscriberZeroArm);
  
  nh.advertise(pub_arm_values);

  moveToZero();
}

long timeBlinked = 0;
void blinkLed() {
  timeBlinked = millis();
  digitalWrite(ledPin, HIGH);
}

void loop() {
  nh.spinOnce();

  float baseDemand = motor_data[0];  // demand for the base motor - in degrees
  float armDemand[2]; // demand for the two arm motors - in degrees
  armDemand[0] = motor_data[1];
  armDemand[1] = motor_data[2];

  updateBasePosition(baseDemand);
  updateMotorPower(armDemand);

  if(millis() - prevMillis > 100) {
    arm_values.data[0] = baseAngle;
    arm_values.data[1] = encoderAngle[0];
    arm_values.data[2] = encoderAngle[1];
  
    pub_arm_values.publish(&arm_values);
  
    prevMillis = millis();
  }

  if (millis() - timeBlinked > 30) {
     digitalWrite(ledPin, LOW);
  }
}
