import subprocess
import signal
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("no_cameras")
args = parser.parse_args()
print("N:", args.no_cameras)

subprocess.call("pkill gst-launch-1.0", shell=True)

camera_nos = [str(i + 10) for i in range(int(args.no_cameras))]

subprocess.call("modprobe -r v4l2loopback", shell=True)
subprocess.call("modprobe v4l2loopback video_nr=" + ",".join(camera_nos) + " exclusive_caps=" + ",".join(["1"] * len(camera_nos)) + " max_buffers=3", shell=True)

gst_processes = []

for i in range(int(args.no_cameras)):
    command = 'gst-launch-1.0 udpsrc port={} caps="application/x-rtp, payload=96, clock-rate=90000" ! rtpjitterbuffer ! rtph264depay ! avdec_h264 ! videoconvert ! v4l2sink device=/dev/video{} sync=0'.format(8000 + i, 10 + i)
    gst_processes.append(subprocess.Popen(command, shell=True))
    print(command)

try:
    gst_processes[0].wait()
except KeyboardInterrupt:
    for proc in gst_processes:
        proc.send_signal(signal.SIGINT)