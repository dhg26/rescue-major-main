# example code for using the XM430 motors

from XM430_control_functions import XM430controller
import time

EXT_POS_CTRL = 4
count_per_rev = 4096

deviceName = '/dev/tty.usbserial-FT45BL4X'
XM_id = 2

def moveAngle(ID, angle, timeStop = 0): 
	errorMargin = 2
	targetPos = int(count_per_rev * (angle/360))

	print("target angle: ", angle)
	XM.writePosition(ID, targetPos)
	stable = 0
	timer = time.time()
	while 1:
		currPos = XM.readPosition(XM_id, printSetting=False)
		if abs(currPos - targetPos) <= errorMargin:
			stable += 1

		if stable >= 10: 
			break
			
		if timeStop != 0 and time.time() - timer > timeStop:
			break
	print("target achieved\n")



if __name__ == '__main__':	

	# make servo instance
	XM = XM430controller(deviceName)


	XM.initCommunication()

	# Set extended position control mode
	XM.setDriveMode(XM_id)
	XM.setDriveMode(XM_id)
	XM.setOperatingMode(XM_id, MODE = EXT_POS_CTRL)

	XM.enableTorque(XM_id)

	moveAngle(XM_id, 0)
	t = time.time()

	XM.setDriveTime(XM_id, 8000)
	moveAngle(XM_id, 360*8)
	print(time.time() -  t)

	XM.setDriveTime(XM_id, 0)
	moveAngle(XM_id, 0)

	XM.disableTorque(XM_id)